// Plugin réalisé par Violette NYS, en mai 2017.

function imagepluginmodal(options){
    
    // paramétrage du plugin (1ère partie)
    var options = (options != undefined)? options : {};
    options.crop_width = (options.crop_width != undefined) ? options.crop_width : 200;
    options.crop_max_height = (options.crop_max_height != undefined) ? options.crop_max_height : 400;
    
    var titre = (options.titre != undefined) ? options.titre : "Outil Image";
    var texteHeader = (options.texteHeader != undefined) ? options.texteHeader : [];
    var texteFooter = (options.texteFooter != undefined)? options.texteFooter : [];
    
    var headerString = "";
    texteHeader.forEach(function(ligne){
        headerString += ligne + "<br>";
    });
    
    var footerString = (options.dimensionsToFooter === false)? "" : "Dimensions : " + options.crop_width + "px de large sur " + options.crop_max_height +"px de haut maximum.";
    texteFooter.forEach(function(ligne){
        footerString += "<br>"+ligne;
    });

    // EVENTLISTENERS appliqués aux éléments lançant le plugin (enlevés une fois les éléments nécessaires au plugin créés)
    document.querySelectorAll('.imageplugin').forEach(function(pluginTrigger){
        pluginTrigger.addEventListener('click', setmodal);
        pluginTrigger.style.cursor = 'pointer';
    });

    //region INITIALISATION FENÊTRE MODALE
    function setmodal(){

        // DETECTION DES DIMENSIONS DE LA FENETRE DU NAVIGATEUR
        var windowWidth = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        var windowHeight = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;

        var smallDevice = (windowHeight <= options.crop_max_height + 200 || windowWidth <= options.crop_width + 300);

        // CREATION DE LA MODALE ET DE SES COMPOSANTS
        var modal = document.createElement('div');
        modal.id = "imgpluginmodal";
        modal.style.display = "block";
        modal.style.position = "fixed";
        modal.style.zIndex = "3000";
        modal.style.left = 0;
        modal.style.top = 0;
        modal.style.width = "100%";
        modal.style.height = "100%";
        modal.style.backgroundColor = "rgb(0,0,0)";
        modal.style.backgroundColor = "rgba(0,0,0,0.4)";
        modal.style.overflow = (smallDevice) ? 'auto' : 'hidden'; // ajout de barres de défilement si petite fenêtre uniquement

        var modalContent = document.createElement('div');
        modalContent.id = 'modalContent';
        modalContent.style.backgroundColor = "#fefefe";
        modalContent.style.margin = "auto";
        modalContent.style.padding = "0";
        modalContent.style.border = "1px solid #888";
        modalContent.style.minWidth = (options.crop_width + 200) + 'px';
        modalContent.style.width = (smallDevice)? '100%' : "80%";
        modalContent.style.marginTop = (smallDevice)? 0 : "5%";
        modalContent.style.borderRadius = "17px";


        var modalHeader = document.createElement('div');
        modalHeader.style.backgroundColor = "#004D95";
        modalHeader.style.color = "white";
        modalHeader.style.padding = "16px";
        modalHeader.style.borderRadius = "17px 17px 0 0";

        var modalBody = document.createElement('div');
        modalBody.style.padding = "16px";

        var modalFooter = document.createElement('div');
        modalFooter.style.backgroundColor = "#004D95";
        modalFooter.style.color = "white";
        modalFooter.style.padding = "16px";
        modalFooter.style.borderRadius = "0 0 17px 17px";


        // ENFANTS DU MODAL HEADER
        var closebtn = document.createElement('span');
        closebtn.textContent = '\327'; // équivalent JS du &times; en html
        closebtn.style.color = "white";
        closebtn.style.float = "right";
        closebtn.style.fontSize = "28px";
        closebtn.style.fontWeight = "bold";
        closebtn.style.cursor = "pointer";
        modalHeader.appendChild(closebtn);

        var modalTitle = document.createElement('h1');
        modalTitle.textContent = (titre == undefined || titre == "")? "Outil image" : titre;
        modalTitle.style.marginBottom = '15px';
        modalHeader.appendChild(modalTitle);

        if(headerString != ""){
            var headerP = document.createElement('p');
            headerP.innerHTML = headerString;
            modalHeader.appendChild(headerP);
        }

        // ENFANTS DU MODAL BODY
        var pluginZone = document.createElement('div');
        pluginZone.id = 'pluginZone';
        modalBody.appendChild(pluginZone);

        // ENFANTS DU MODAL FOOTER
        if(footerString != ""){
            var footerP = document.createElement('p');
            footerP.innerHTML = footerString;
            modalFooter.appendChild(footerP);
        }
        

        // ENFANTS DU MODAL CONTENT
        modalContent.appendChild(modalHeader);
        modalContent.appendChild(modalBody);
        modalContent.appendChild(modalFooter);

        // FINITION FENÊTRE MODALE
        modal.appendChild(modalContent);
        document.body.appendChild(modal);


        // EVENTLISTENERS

        // fermeture de la fenêtre modale si on clique sur la croix en haut à droite
        closebtn.addEventListener('click', function(e){
            modal.style.display = "none";
        });

        // ... ou si on clique en dehors du modalContent
        window.onclick = function(e){
            if(e.target == modal){
                modal.style.display = "none";
            }
        };

        // ouverture de la fenêtre modale si on clique sur l'un des déclencheurs (image, bouton)
        document.querySelectorAll('.imageplugin').forEach(function(pluginTrigger){
            pluginTrigger.addEventListener('click', function(e){
                modal.style.display = "block";
            });
        });


        // SUPPRESSION EVENTLISTENERS UNE FOIS LA MODALE CREEE
        document.querySelectorAll('.imageplugin').forEach(function(pluginTrigger){
            pluginTrigger.removeEventListener('click', setmodal);
        });


        // INITIALISATION DU PLUGIN DANS LA MODALE
        imageplugin(options);
    }
    //endregion
}

function imageplugin(options){
    
    // paramétrage du plugin (2ème partie)
    var crop_width = options.crop_width;
    var crop_max_height = options.crop_max_height;


    // DECLARATION DE VARIABLES utiles à l'ensemble des fonctions ci-après
    var zones = {
        'plugin': {},
        'full': {},
        'crop': {},
        'img': {},
        'startResize' : {},
        'startMove' : {},
        'zoom': {}
    };

    var img;

    // INJECTION DE <STYLE> DANS LE HEAD
    // Uniformisation du calcul de width et height pour tous les navigateurs: avec box-sizing: border-box, les propriétés width et height incluent le contenu, le remplissage (padding), la bordure, mais pas la marge.
    var style = document.createElement('style');
    style.type = 'text/css';
    style.textContent = '#imgpluginmodal * { box-sizing: border-box; font-family:Trebuchet,"Trebuchet MS", Arial, Helvetica, Sans-serif; margin:0; padding:0;} #imgpluginmodal p { text-align:justify; font-size:10pt;} #imgpluginmodal h1 {text-align:left;font-size:16pt;font-weight:bold;}';
    var head = document.head || document.querySelector('head');
    head.appendChild(style);

    /////////////////////////////////////////////////////////////////
    ///////////// INITIALISATION DES DIFFERENTES ZONES //////////////
    ///// (création, style, insertion DOM, écoute d'événements) /////
    /////////////////////////////////////////////////////////////////

    //region initialisation zones

    zoneInit('pluginZone');
    zoneInit('fullZone');
    zoneInit('cropZone');
    zoneInit('toolsZone');
    zoneInit('fullImg');

    function zoneInit(idZone){

        switch(idZone){

            case 'pluginZone':

                // RECUPERATION DOM
                var zoneElmt = document.getElementById('pluginZone');

                // STOCKAGE
                zones.plugin.elmt = zoneElmt;

                // STYLE
                var parentPluginZone = zoneElmt.parentElement;
                parentPluginZone.style.padding = '0';
                var parentWidth = parentPluginZone.clientWidth;

                zoneElmt.style.position = "relative";
                zoneElmt.style.display = 'flex';
                zoneElmt.style.flexDirection = 'row';
                zoneElmt.style.justifyContent = 'center';
                zoneElmt.style.alignItems = 'center';
                zoneElmt.style.width =  parentWidth + "px";

                break;

            case 'fullZone':

                // CREATION
                zoneElmt = document.createElement('div');
                zoneElmt.id = idZone;

                // INSERTION DOM
                zones.plugin.elmt.appendChild(zoneElmt);

                // STOCKAGE
                zones.full.elmt = zoneElmt;
                zones.full.width = zoneElmt.parentElement.clientWidth - 200;
                zones.full.height = crop_max_height + 20;

                // STYLE
                zoneElmt.style.width = zones.full.width + 'px';
                zoneElmt.style.minWidth = zones.full.width + 'px';
                zoneElmt.style.height = zones.full.height + 'px';
                zoneElmt.style.border = '1px solid red';
                zoneElmt.style.display = 'block';
                zoneElmt.style.position = 'relative';
                zoneElmt.style.textAlign = 'center';
                zoneElmt.style.overflow = 'hidden';


                // EVENTS LISTENERS

                // empêche le comportement par défaut du navigateur, qui est d'ouvrir le fichier automatiquement
                window.addEventListener('dragover', function(e){ e = e || event; e.preventDefault();}, false);
                window.addEventListener('drop', function(e){
                    e = e || event; e.preventDefault();
                    var newFile = e.dataTransfer.files[0]; // on récupère le fichier
                    var pattern = new RegExp(/^image\//);
                    if(!pattern.test(newFile.type)){
                        return false;
                    }
                    else{
                        initDroppedImage(newFile);
                    }
                }, false);

                break;

            case 'cropZone':

                // CREATION
                zoneElmt = document.createElement('div'); // zone de découpe
                zoneElmt.id = idZone;
                var resizeHandle = document.createElement('span'); // poignée de redimensionnement en hauteur
                resizeHandle.id = 'resizeHandle';

                // STOCKAGE
                zones.crop.elmt = zoneElmt;
                zones.crop.width = crop_width;
                zones.crop.height = (zones.crop.height == undefined)? crop_max_height : zones.crop.height;
                zones.crop.top = (zones.full.height - zones.crop.height) / 2;
                zones.crop.left = (zones.full.width - crop_width) / 2;

                // STYLE
                zoneElmt.style.width = crop_width + 'px';
                zoneElmt.style.height = zones.crop.height + 'px';
                zoneElmt.style.border = '2px solid green';
                zoneElmt.style.position = 'absolute';
                zoneElmt.style.top = zones.crop.top + 'px';
                zoneElmt.style.left = zones.crop.left + 'px';
                zoneElmt.style.zIndex = 99;
                zoneElmt.style.pointerEvents = 'none'; // permet le changement de curseur au survol de l'image qui se trouvera derrière ce div

                resizeHandle.style.width = '10px';
                resizeHandle.style.height = '10px';
                resizeHandle.style.backgroundColor = 'white';
                resizeHandle.style.border = '1px solid green';
                resizeHandle.style.cursor = 'ns-resize';
                resizeHandle.style.position = 'absolute';
                resizeHandle.style.top = (crop_max_height - 7) + 'px';
                resizeHandle.style.left = ((crop_width / 2) - 5) + 'px';
                resizeHandle.style.zIndex = 101;
                resizeHandle.style.pointerEvents = 'auto'; // autorise le changement de curseur alors que le div parent (cropZone) à cette propriété à 'none'

                // INSERTION DOM
                zoneElmt.appendChild(resizeHandle);
                zones.full.elmt.appendChild(zoneElmt);

                break;

            case 'toolsZone':

                // <NAV>
                zoneElmt = document.createElement('nav');

                // STYLE
                zoneElmt.style.display = 'flex';
                zoneElmt.style.flexDirection = 'column';
                zoneElmt.style.justifyContent = 'flex-start';
                zoneElmt.style.alignItems = 'center';
                zoneElmt.style.width = '150px';
                zoneElmt.style.maxWidth = '150px';

                // <BUTTON>
                var buttons = ['move','zoom', 'magnet', 'cropit'];

                buttons.forEach(function(idBtn){

                    // TEXT NODES + EVENTS LISTENERS
                    switch(idBtn){

                        case 'zoom':
                            // CREATION
                            var btn = document.createElement('div');
                            btn.id = 'zoom';

                            var zoomIn = document.createElement('input');
                            zoomIn.type = 'image';
                            zoomIn.src = "./icones/zoomin.png";
                            zoomIn.alt = "Zoomer";
                            zoomIn.width = 30;
                            zoomIn.addEventListener('mouseover', function(e){e.currentTarget.src = "./icones/zoominhover.png";});
                            zoomIn.addEventListener('mouseout', function(e){e.currentTarget.src = "./icones/zoomin.png";});
                            zoomIn.addEventListener('click', function(e){zoom(e, 'in');});

                            var zoomSlider = document.createElement('input');
                            zoomSlider.id = 'zoomSlider';
                            zoomSlider.type = 'range';
                            zoomSlider.value = '0';
                            zoomSlider.style.width = '110px';
                            zoomSlider.addEventListener('change', function(e){
                                zoom(e, 'slide');
                            });

                            var zoomOut = document.createElement('input');
                            zoomOut.type = 'image';
                            zoomOut.src = "./icones/zoomout.png";
                            zoomOut.alt = "Dézoomer";
                            zoomOut.width = 30;
                            zoomOut.title = "Diminuer les dimensions de l'image";
                            zoomOut.addEventListener('mouseover', function(e){e.currentTarget.src = "./icones/zoomouthover.png";});
                            zoomOut.addEventListener('mouseout', function(e){e.currentTarget.src = "./icones/zoomout.png";});
                            zoomOut.addEventListener('click', function(e){zoom(e, 'out');});

                            btn.appendChild(zoomOut);
                            btn.appendChild(zoomSlider);
                            btn.appendChild(zoomIn);

                            break;

                        case 'move':
                            var btn = document.createElement('div');
                            btn.id = 'move';
                            btn.style.display = 'flex';
                            btn.style.flexDirection = 'row';
                            btn.style.flexWrap = 'wrap';
                            btn.style.width = '90px';
                            btn.style.height = '90px';

                            var moveUp = document.createElement('input');
                            moveUp.type = 'image';
                            moveUp.src = "./icones/deplacement/haut.png";
                            moveUp.alt = "haut";
                            moveUp.width = 90;
                            moveUp.height = 35;
                            moveUp.addEventListener('mouseover', function(e){e.currentTarget.src = "./icones/deplacement/hautgris.png";});
                            moveUp.addEventListener('mouseout', function(e){e.currentTarget.src = "./icones/deplacement/haut.png";});

                            moveUp.addEventListener('click', function(){
                                move('up');

                            });

                            var moveLeft = document.createElement('input');
                            moveLeft.type = 'image';
                            moveLeft.src = "./icones/deplacement/gauche.png";
                            moveUp.alt = "gauche";
                            moveLeft.width = 45;
                            moveLeft.height = 20;
                            moveLeft.addEventListener('mouseover', function(e){e.currentTarget.src = "./icones/deplacement/gauchegris.png";});
                            moveLeft.addEventListener('mouseout', function(e){e.currentTarget.src = "./icones/deplacement/gauche.png";});
                            moveLeft.addEventListener('click', function(){
                                move('left');
                            });

                            var moveRight = document.createElement('input');
                            moveRight.type = 'image';
                            moveRight.src = "./icones/deplacement/droite.png";
                            moveRight.alt = "droite";
                            moveRight.width = 45;
                            moveRight.height = 20;
                            moveRight.addEventListener('mouseover', function(e){e.currentTarget.src = "./icones/deplacement/droitegris.png";});
                            moveRight.addEventListener('mouseout', function(e){e.currentTarget.src = "./icones/deplacement/droite.png";});
                            moveRight.addEventListener('click', function(){
                                move('right');
                            });

                            var moveDown = document.createElement('input');
                            moveDown.type = 'image';
                            moveDown.src = "./icones/deplacement/bas.png";
                            moveDown.alt = "bas";
                            moveDown.width = 90;
                            moveDown.height = 35;
                            moveDown.addEventListener('mouseover', function(e){e.currentTarget.src = "./icones/deplacement/basgris.png";});
                            moveDown.addEventListener('mouseout', function(e){e.currentTarget.src = "./icones/deplacement/bas.png";});
                            moveDown.addEventListener('click', function(){
                                move('down');
                            });

                            btn.appendChild(moveUp);
                            btn.appendChild(moveLeft);
                            btn.appendChild(moveRight);
                            btn.appendChild(moveDown);

                            break;

                        case 'magnet':
                            var btn = document.createElement('div');
                            btn.id = 'magnet';
                            btn.style.width = '120px';
                            btn.style.height = '120px';
                            btn.style.display = 'flex';
                            btn.style.flexDirection = 'row';
                            btn.style.flexWrap = 'wrap';

                            var magnetTop = document.createElement('input');
                            magnetTop.type = 'image';
                            magnetTop.src = './icones/magnet/haut.png';
                            magnetTop.alt = 'coller en haut';
                            magnetTop.width = 120;
                            magnetTop.height = 40;
                            magnetTop.addEventListener('mouseover', function(e){ e.currentTarget.src = "./icones/magnet/hauthover.png";});
                            magnetTop.addEventListener('mouseout', function(e){ e.currentTarget.src = "./icones/magnet/haut.png";});
                            magnetTop.addEventListener('click', function(){ magnetize('top');});

                            var magnetLeft = document.createElement('input');
                            magnetLeft.type = 'image';
                            magnetLeft.src = './icones/magnet/gauche.png';
                            magnetLeft.alt = 'coller à gauche';
                            magnetLeft.width = 40;
                            magnetLeft.height = 40;

                            magnetLeft.addEventListener('mouseover', function(e){ e.currentTarget.src = "./icones/magnet/gauchehover.png";});
                            magnetLeft.addEventListener('mouseout', function(e){ e.currentTarget.src = "./icones/magnet/gauche.png";});
                            magnetLeft.addEventListener('click', function(){magnetize('left');});

                            var magnetAuto = document.createElement('input');
                            magnetAuto.type = 'image';
                            magnetAuto.src = './icones/magnet/milieu.png';
                            magnetAuto.alt = 'magnet auto';
                            magnetAuto.width = 40;
                            magnetAuto.height = 40;
                            magnetAuto.addEventListener('mouseover', function(e){e.currentTarget.src = './icones/magnet/milieuhover.png';});
                            magnetAuto.addEventListener('mouseout', function(e){e.currentTarget.src = './icones/magnet/milieu.png';});
                            magnetAuto.addEventListener('click', function(){magnetize('auto');});

                            var magnetRight = document.createElement('input');
                            magnetRight.type = 'image';
                            magnetRight.src = './icones/magnet/droite.png';
                            magnetRight.alt = 'coller à droite';
                            magnetRight.width = 40;
                            magnetRight.height = 40;
                            magnetRight.addEventListener('mouseover', function(e){ e.currentTarget.src = "./icones/magnet/droitehover.png";});
                            magnetRight.addEventListener('mouseout', function(e){ e.currentTarget.src = "./icones/magnet/droite.png";});
                            magnetRight.addEventListener('click', function(){magnetize('right');});


                            var magnetBottom = document.createElement('input');
                            magnetBottom.type = 'image';
                            magnetBottom.src = './icones/magnet/bas.png';
                            magnetBottom.alt = 'coller en bas';
                            magnetBottom.width = 120;
                            magnetBottom.height = 40;
                            magnetBottom.addEventListener('mouseover', function(e){ e.currentTarget.src = "./icones/magnet/bashover.png";});
                            magnetBottom.addEventListener('mouseout', function(e){ e.currentTarget.src = "./icones/magnet/bas.png";});
                            magnetBottom.addEventListener('click', function(){magnetize('bottom');});

                            btn.appendChild(magnetTop);
                            btn.appendChild(magnetLeft);
                            btn.appendChild(magnetAuto);
                            btn.appendChild(magnetRight);
                            btn.appendChild(magnetBottom);
                            break;

                        case 'cropit':
                            var btn = document.createElement('button');
                            btn.textContent = 'VALIDER';
                            btn.classList += 'bouton';
                            btn.addEventListener('click', cropit);
                            break;

                        case 'preview':
                            var btn = document.createElement('button');
                            btn.textContent = 'PREVIEW';
                            btn.addEventListener('click', showCrop);
                            break;

                        case 'rotate':
                            var btn = document.createElement('input');
                            btn.type = 'image';
                            btn.src = './icones/rotate.png';
                            btn.alt = 'pivoter';
                            btn.width = 60;
                            btn.height = 60;
                            btn.addEventListener('mouseover', function(e){ e.currentTarget.src = "./icones/rotatehover.png";});
                            btn.addEventListener('mouseout', function(e){ e.currentTarget.src = "./icones/rotate.png";});

                            btn.addEventListener('click', rotate90);
                            break;

                        default: console.log('pas Id?'); break;
                    }

                    btn.style.margin = '5px';

                    // INSERTION DANS <NAV>
                    zoneElmt.appendChild(btn);
                });

                // INSERTION DOM <NAV> + n * <BUTTON>
                zones.plugin.elmt.appendChild(zoneElmt);
                break;

            case 'fullImg':

                // IMPORT DE L'IMAGE DE DEPART
                if(document.querySelector('img.imageplugin') != undefined){
                    zoneElmt = document.createElement('img');
                    zoneElmt.id = idZone;
                    zoneElmt.src = document.querySelector('img.imageplugin').src;
                    zones.full.elmt.appendChild(zoneElmt);
                    img = zoneElmt;

                    initImgDimensions();
                }
                break;

            default: break;
        }
    }
    //endregion


    //////////////////////////////////////////////
    ///// FONCTIONS D'INITIALISATION D'IMAGE /////
    //////////////////////////////////////////////

    //region initialisation image

    function initImgDimensions(){
        
        var imgNaturalWidth = img.naturalWidth;
        var imgNaturalHeight = img.naturalHeight;
        
        if(imgNaturalWidth < crop_width){
            alert("L'image importée a une largeur d'origine inférieure à celle de la zone de découpe. Il sera impossible de la traiter. Veuillez sélectionner une autre image svp.");
        } 
        // RECUP ELEMENTS
        var cropZone = document.getElementById('cropZone');
        var resizeHandle = document.getElementById('resizeHandle');


        // DIMENSIONS IMAGE
        img.style.width = crop_width + "px";
        zones.img.width = crop_width;
        img.style.maxHeight = crop_max_height + "px";
        var actualHeight = Math.round(img.clientHeight);
        zones.img.height = actualHeight;
        img.style.height = actualHeight + 'px';
        img.style.maxHeight = img.naturalHeight + 'px'; // dimensions max (zoom) = taille d'origine de l'image
        img.style.maxWidth = img.naturalWidth + 'px';
        zones.img.ratio = zones.img.width / zones.img.height;

        // DIMENSIONS ZONE DE DECOUPE
        cropZone.style.height = actualHeight+"px";
        zones.crop.height = actualHeight;

        // REGLAGE DES VALEURS MIN et MAX DU SLIDER ZOOM
        zones.zoom.min = crop_width;
        zones.zoom.max = img.naturalWidth;
        var zoomSlider = document.getElementById('zoomSlider');
        zoomSlider.min = zones.zoom.min;
        zoomSlider.max = zones.zoom.max;

        // POSITIONNEMENT
        img.style.position = 'absolute';
        var topPos = (zones.full.height - zones.img.height) / 2;
        var leftPos = zones.crop.left;
        zones.img.left = leftPos;
        zones.img.top = topPos;
        zones.crop.top = topPos;
        img.style.left = leftPos + 'px';
        img.style.top = topPos + 'px';
        cropZone.style.top = topPos + 'px';
        resizeHandle.style.top = (resizeHandle.parentElement.clientHeight - 5) + 'px';

        // CURSEUR DEPLACEMENT IMAGE
        img.style.cursor = 'move';

        // INITIALISATION DU DEGRE DE ROTATION
        zones.img.rotation = 0;

        // EVENTLISTENER POIGNEE REDIMENSIONNEMENT ZONE DE DECOUPE + DEPLACEMENT IMAGE
        document.addEventListener('mousedown', function(e){
            switch(e.target.id){
                case 'resizeHandle': startResizeCropZone(e); break;
                case 'fullImg': startMoveImage(e); break;
                default: break;
            }
        });

        // OMBRAGE A L'EXTERIEUR DE LA ZONE DE DECOUPE
        var shadow = (document.getElementById('shadow') == undefined) ? document.createElement('div') : document.getElementById('shadow');
        if(document.getElementById('shadow') == undefined)
            shadow.id='shadow';
        shadow.style.outline = 'rgba(0,0,0,0.6) solid 450px';
        shadow.style.zIndex = 88;
        shadow.style.width = '100%';
        shadow.style.height = '100%';
        zones.crop.elmt.appendChild(shadow);

    }

    function initDroppedImage(file){

        var cropZone = document.getElementById('cropZone');

        // si une image est déjà là, on la retire pour mettre la nouvelle (1 image à la fois!)
        if(img != undefined){
            img.parentNode.removeChild(img);
        }
        // on crée la nouvelle image et on l'insère dans la dropzone une fois le fichier chargé
        img = document.createElement('img');
        img.id = 'fullImg';

        img.src = window.URL.createObjectURL(file);
        img.onload = function(){
            document.getElementById('fullZone').appendChild(img);
            initImgDimensions();
        }

    }
    //endregion

    ////////////////////////////////////////////////////////////////
    ///// FONCTIONS DE REDIMENSIONNEMENT DE LA ZONE DE DECOUPE /////
    ////////////////////////////////////////////////////////////////

    //region redimensionnement cropZone

    function startResizeCropZone(e){
        e.preventDefault();
        e.stopPropagation();

        // RECUP POSITION INITIALE SOURIS
        zones.startResize.mouse_y = e.clientY || e.pageY;

        // EVENTLISTENERS SOURIS
        document.addEventListener('mousemove', resizeCropZone);
        document.addEventListener('mouseup', endResizeCropZone);
    }

    function resizeCropZone(e){

        // RECUP POSITION SOURIS - CALCUL DIMENSIONS
        var new_y = e.clientY || e.pageY;
        var diff = new_y - zones.startResize.mouse_y;
        var newHeight = (zones.crop.height + diff <= crop_max_height) ? zones.crop.height + diff : crop_max_height ;

        // STYLE: DIMENSIONS ET POSITIONNEMENT
        document.getElementById('cropZone').style.height = newHeight + 'px';
        document.getElementById('resizeHandle').style.top = (newHeight - 7) + 'px';

        // VERIF. SI TAILLE MAX ATTEINTE
        if(newHeight == crop_max_height){
            endResizeCropZone(e, newHeight);
            alert("Vous avez atteint la hauteur maximale");
        }
    }

    function endResizeCropZone(e, newHeight){

        // SUPPRESSION EVENTLISTENERS SOURIS
        e.preventDefault();
        document.removeEventListener('mousemove', resizeCropZone);
        document.removeEventListener('mouseup', endResizeCropZone);

        // CALCUL HAUTEUR ZONE DE DECOUPE
        var cropZone = document.getElementById('cropZone');
        var cropHeight = (newHeight != undefined) ? newHeight : Number(cropZone.style.height.substr(0, cropZone.style.height.length - 2));
        zones.crop.height = cropHeight;

        // SI DEPASSEMENT, REPOSITIONNEMENT ZONE DE DECOUPE ET IMAGE
        if(cropHeight >= zones.full.height - zones.crop.top){
            var newTopPos = (zones.full.height - cropHeight) / 2;
            var diffTopPos = zones.crop.top - newTopPos;
            cropZone.style.top = newTopPos + 'px';
            zones.crop.top = newTopPos;
            zones.img.top = zones.img.top - diffTopPos;
            img.style.top = zones.img.top + 'px';

        }

    }
    //endregion

    //////////////////////////////////////////
    ///// FONCTIONS DE ZOOM DANS L'IMAGE /////
    //////////////////////////////////////////

    //region zoom image

    function zoom(e, typeZoom){
        if(img != undefined){

            switch(typeZoom){
                case 'in': var zoomVal = 1.05; break;
                case 'out': var zoomVal = 0.95; break;
                case 'slide': var zoomVal = e.currentTarget.value / zones.zoom.min; break;
                default: return;
            }

            // REDIMENSIONNEMENT IMAGE (avec calcul des nouvelles positions)
            if(typeZoom == 'slide'){
                var nextWidth = parseInt(e.currentTarget.value); // value retourne un string -> important de parser pour récupérer un int!!!
            }
            else{
                // nouvelles dimensions déterminées en tenant compte du min et du max de zoom.
                var nextWidth = Math.round(zones.img.width * zoomVal);
                nextWidth = (nextWidth > zones.zoom.min && nextWidth < zones.zoom.max) ? nextWidth : (nextWidth < zones.zoom.min)? zones.zoom.min : zones.zoom.max;
            }

            var diffLeft = (nextWidth - zones.img.width) / 2;
            zones.img.width = nextWidth;
            img.style.width = zones.img.width + 'px';

            var nextHeight = zones.img.width / zones.img.ratio;
            var diffTop = (nextHeight - zones.img.height) / 2;
            zones.img.height = nextHeight;
            img.style.height = zones.img.height + 'px';

            if(typeZoom != 'slider'){
                document.getElementById('zoomSlider').value = zones.img.width;
            }

            // POSITIONNEMENT IMAGE REDIMENSIONNEE
            zones.img.top -= diffTop;
            zones.img.left -= diffLeft;
            img.style.top = zones.img.top + 'px';
            img.style.left = zones.img.left + 'px';

            // si l'image dé-zoomée est trop éloignée de la zone de découpe, on la recentre
            if(zones.img.top + zones.img.height < zones.crop.top || zones.img.top > zones.crop.top + zones.crop.height){
                zones.img.top = zones.crop.top + zones.crop.height - zones.img.height;
                img.style.top = zones.img.top + 'px';
            }
            if(zones.img.left + zones.img.width < zones.crop.left || zones.img.left > zones.crop.left + zones.crop.width){
                zones.img.left = zones.crop.left + zones.crop.width - zones.img.width;
                img.style.left = zones.img.left + 'px';
            }
        }
    }
    //endregion


    ///////////////////////////////////////////////
    ///// FONCTIONS DE DEPLACEMENT DE L'IMAGE /////
    ///////////////////////////////////////////////

    //region déplacement image

    function startMoveImage(e){
        e.preventDefault();
        e.stopPropagation();

        // RECUP POSITION INITIALE SOURIS
        zones.startMove.mouse_y = e.clientY || e.pageY;
        zones.startMove.mouse_x = e.clientX || e.pageX;

        // EVENTLISTENERS SOURIS
        document.addEventListener('mousemove', moveImage);
        document.addEventListener('mouseup', endMoveImage);
    }

    function moveImage(e){

        // RECUP POSITION SOURIS - CALCUL DIMENSIONS
        var new_x = e.clientX || e.pageX;
        var new_y = e.clientY || e.pageY;
        var diff_x = new_x - zones.startMove.mouse_x;
        var diff_y = new_y - zones.startMove.mouse_y;

        var newLeft = zones.img.left + diff_x;
        var newTop = zones.img.top + diff_y;

        // STYLE: POSITIONNEMENT IMAGE
        img.style.top = ((newTop < -(zones.img.height - 30) || newTop > zones.full.height - 30) ? ((newTop < -(zones.img.height - 30))? -(zones.img.height - 30) : zones.full.height - 30) : newTop) + 'px'; // empêche de sortir l'image du cadre par le haut ou par le bas
        img.style.left = ((newLeft < -(zones.img.width - 30) || newLeft > zones.full.width - 30) ? ((newLeft < -(zones.img.width - 30))?  -(zones.img.width - 30) : zones.full.width - 30) : newLeft) + 'px'; // empêche de sortir l'image du cadre par la gauche ou par la droite
    }

    function endMoveImage(e){
        // SUPPRESSION EVENTLISTENERS SOURIS SUR L'IMAGE
        e.preventDefault();
        document.removeEventListener('mousemove', moveImage);
        document.removeEventListener('mouseup', endMoveImage);

        // STOCKAGE DES NOUVELLES VALEURS DE POSITIONNEMENT
        zones.img.top = Number(img.style.top.substr(0, img.style.top.length - 2));
        zones.img.left = Number(img.style.left.substr(0, img.style.left.length - 2));
    }

    function move(direction){
        if(img != undefined){
            switch(direction){
                case 'up':
                    img.style.top = (zones.img.top -= 5)+'px';
                    break;
                case 'down':
                    img.style.top = (zones.img.top += 5)+'px';
                    break;
                case 'left':
                    img.style.left = (zones.img.left -= 5)+'px';
                    break;
                case 'right':
                    img.style.left = (zones.img.left += 5)+'px';
                    break;
                default:
                    break;
            }
        }
    }
    //endregion

    //////////////////////////////////////////////////
    ///// FONCTIONS AIMANT BORDS IMAGE - DECOUPE /////
    //////////////////////////////////////////////////

    //region aimant

    function magnetize(border) {

        if(img != undefined){
            if (border == 'auto') {
                if (zones.img.top > zones.crop.top) {
                    magnetize('top');
                }
                else if (zones.img.top + zones.img.height < zones.crop.top + zones.crop.height) {
                    magnetize('bottom');
                }

                if (zones.img.left > zones.crop.left) {
                    magnetize('left');
                }
                else if (zones.img.left + zones.img.width < zones.crop.left + zones.crop.width) {
                    magnetize('right');
                }
            }
            else {
                // EN CHANTIER...(à vérifier, mais semble déjà ok)
                var diff = (zones.img.rotation % 180 == 0) ? 0 : ((zones.img.width - zones.img.height) / 2);

                switch (border) {
                    case 'top':
                        zones.img.top = zones.crop.top + diff;
                        img.style.top = zones.img.top + 'px';
                        break;
                    case 'left':
                        zones.img.left = zones.crop.left - diff;
                        img.style.left = zones.img.left + 'px';
                        break;
                    case 'right':
                        zones.img.left = zones.crop.left + zones.crop.width - zones.img.width + diff;
                        img.style.left = zones.img.left + 'px';
                        break;
                    case 'bottom':
                        zones.img.top = zones.crop.top + zones.crop.height - zones.img.height - diff;
                        img.style.top = zones.img.top + 'px';
                    default:
                        break;
                }
            }
        }
    }
    //endregion

    /////////////////////////////////////////////
    ///// FONCTIONS DE ROTATION DE L'IMAGE  /////
    /////////////////////////////////////////////

    function rotate90(){
        // EN CHANTIER...

        if(img != undefined){
            zones.img.rotation = (zones.img.rotation + 90) % 360;
            img.style.transform = 'rotate('+ zones.img.rotation + 'deg)';
            var diff = (zones.img.width - zones.img.height) / 2;

            switch(zones.img.rotation) {
                case 90:
                case 270:
                    zones.img.top += diff;
                    zones.img.left -= diff;
                    break;
                case 0:
                case 180:
                    zones.img.top -= diff;
                    zones.img.left += diff;
                    break;
                default:
                    break;
            }
            img.style.top = zones.img.top + 'px';
            img.style.left = zones.img.left + 'px';

        }
    }

    ///////////////////////////////////////////////////////
    ///// FONCTIONS DE DECOUPE ET D'EXPORT DE L'IMAGE /////
    ///////////////////////////////////////////////////////

    //region découpe et export image

    function cropit(){

        if(img == undefined){
            alert("Glissez - déposez l'image de votre choix dans la zone de redimensionnement pour commencer.");
        }
        else if(
            zones.img.top <= zones.crop.top
            && zones.img.left <= zones.crop.left
            && (zones.img.top + zones.img.height) >= (zones.crop.top + zones.crop.height)
            && (zones.img.width + zones.img.left) >= (zones.crop.left + zones.crop.width)
        ) { // on vérifie que l'image couvre bien toute la zone de découpe
            var crop_canvas = document.createElement('canvas');
            crop_canvas.width = zones.crop.width;
            crop_canvas.height = zones.crop.height;

            // paramètres
            var widthRatio = img.naturalWidth / img.clientWidth;
            var heightRatio = img.naturalHeight / img.clientHeight;

            var sx = Math.round(zones.crop.left - zones.img.left) * widthRatio; // coordonnées à partir desquelles couper dans l'image (aux dimensions originales)
            var sy = Math.round(zones.crop.top - zones.img.top) * heightRatio;

            var swidth = zones.crop.width * widthRatio; // dimensions de la découpe dans l'image (aux dimensions originales)
            var sheight = zones.crop.height * heightRatio;

            var dx = 0; // coordonnées du placement dans le canvas
            var dy = 0;
            var dwidth = zones.crop.width;
            var dheight = zones.crop.height;

            var ctx = crop_canvas.getContext('2d');


            if(zones.img.rotation % 360 != 0){
                // EN CHANTIER...
                var degres = zones.img.rotation % 360;
                var angle = degres * Math.PI/180;
                ctx.rotate(angle);

                if(degres == 180){
                    dwidth = -dwidth;
                    dheight = -dheight;
                }
                else if (degres == 90){
                    dwidth = dheight;
                    dheight = -dwidth;
                }
            }

            ctx.drawImage(img, sx, sy, swidth, sheight, dx, dy, dwidth, dheight);

            function drawRotated(degrees){
                // EN CHANTIER...
                if(canvas) document.body.removeChild(canvas);

                canvas = document.createElement("canvas");
                var ctx=canvas.getContext("2d");
                canvas.style.width="20%";

                if(degrees == 90 || degrees == 270) {
                    canvas.width = image.height;
                    canvas.height = image.width;
                } else {
                    canvas.width = image.width;
                    canvas.height = image.height;
                }

                ctx.clearRect(0,0,canvas.width,canvas.height);
                if(degrees == 90 || degrees == 270) {
                    ctx.translate(image.height/2,image.width/2);
                } else {
                    ctx.translate(image.width/2,image.height/2);
                }
                ctx.rotate(degrees*Math.PI/180);
                ctx.drawImage(image,-image.width/2,-image.height/2);

                document.body.appendChild(canvas);
            }

            var canvasData = crop_canvas.toDataURL("image/png");
            saveToFolder(canvasData);
        }
        else{
            alert("Veuillez repositionner l'image ou redimensionner la zone de découpe, afin d'obtenir une image correcte.");
        }
    }

    function openInNewTab(canvasData){
        window.open(canvasData);
    }

    function saveToFolder(canvasData){
        // si aucun autre nom de répertoire n'a pas été renseigné, on envoie vers le répertoire par défaut.
        // idem pour le nom de fichier

        var timestamp = (options.timestamp != undefined) ? options.timestamp : true;
        var fileName = (options.fileName != undefined) ? options.fileName + ((timestamp)? Date.now() : "") + ".png" : "test" + Date.now() + ".png" ; 
        var uploadDir = (options.uploadDir != undefined) ? options.uploadDir : "./photo";

        var xhr = new XMLHttpRequest();
        xhr.open('POST', './imagepluginmodal.php');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        xhr.onload = function(){
            if(xhr.readyState == 4){
                console.log("REPONSE XHR: " + xhr.responseText);
                if(xhr.responseText == "no such folder"){
                    openInNewTab(canvasData);
                }
                //location.reload(true);
                // on affiche l'image qui vient d'être exportée, en ajoutant un paramètre à la fin du nom de fichier afin de forcer le navigateur à recharger l'image (contournement du cache)
                document.querySelector('img.imageplugin').src = "./" + uploadDir+"/"+fileName+"?m="+Date.now();
                document.getElementById('imgpluginmodal').style.display = "none";
            }
            else{
                openInNewTab(canvasData);
                console.error("ERREUR XHR: " + xhr.status);
            }
        };
        xhr.onerror = function(e){
            openInNewTab(canvasData);
            console.error("ERREUR XHR: " + xhr.status);
        };
        
        xhr.send('canvasDataPlugin=' + canvasData + '&uploadDirPlugin=' + uploadDir + '&fileNamePlugin=' + fileName);
    }


    function showCrop(){
        // EN CHANTIER...

        var crop_canvas = document.getElementById('cropPreview');
        console.log("zones.crop.width "+zones.crop.width+" zones.crop.height "+zones.crop.height);
        crop_canvas.width = zones.crop.width;
        crop_canvas.height = zones.crop.height;
        console.log("crop_canvas.width "+crop_canvas.width+" crop_canvas.height "+crop_canvas.height);
        crop_canvas.style.border = '1px solid black';

        // paramètres
        var img =  document.getElementById('fullImg');

        var widthRatio = (img.naturalWidth / img.clientWidth);
        var heightRatio = (img.naturalHeight / img.clientHeight);
        console.log("widthRatio "+widthRatio + " heightRatio " + heightRatio);

        var sx = Math.round(zones.crop.left - zones.img.left) * widthRatio; // coordonnées à partir desquelles couper dans l'image (aux dimensions originales)
        var sy = Math.round(zones.crop.top - zones.img.top) * heightRatio;

        console.log('sx '+sx+' sy '+sy);

        var swidth = zones.crop.width * widthRatio; // dimensions de la découpe dans l'image (aux dimensions originales)
        var sheight = zones.crop.height * heightRatio;

        console.log('swidth '+ swidth + ' sheight '+ sheight);

        var dx = 0; // coordonnées du placement dans le canvas
        var dy = 0;
        var dwidth = zones.crop.width;
        var dheight = zones.crop.height;

        var ctx = crop_canvas.getContext('2d');
        ctx.drawImage(img, sx, sy, swidth, sheight, dx, dy, dwidth, dheight);

    }
    //endregion


    ///////////////////////////////////////////////////////
    ///// SI REDIMENSIONNEMENT FENÊTRE NAVIGATEUR...  /////
    ///////////////////////////////////////////////////////

    //window.addEventListener('resize', resizeZones);
    function resizeZones(){
        // EN CHANTIER....
        var pluginZone = document.getElementById('pluginZone');
        zones.plugin.width = pluginZone.parentElement.clientWidth;
        pluginZone.style.width =  zones.plugin.width + "px";

        var fullZone = document.getElementById('fullZone');
        zones.full.width = zones.plugin.width - 200;
        fullZone.style.width = zones.full.width + 'px';
    }
}