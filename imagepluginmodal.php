<?php

// Violette NYS - le 31 mai 2017

// on détermine le nom du dossier de destination (par défaut ou précisé)
$uploadDir = (isset($_POST['uploadDirPlugin'])) ? $_POST['uploadDirPlugin'] : './photo';

// Si ce dossier existe bien et qu'on a bien reçu les données d'image en POST,
if(is_dir($uploadDir) && isset($_POST['canvasDataPlugin'])){
    // on récupère les données d'image et on les met au bon format
    $data = $_POST['canvasDataPlugin'];
    $filteredData = str_replace('data:image/png;base64,', '', $data);
    $filteredData = str_replace(' ','+', $filteredData);
    $decodedData = base64_decode($filteredData);
    // on détermine le nom qu'on va donner au fichier
    $nomFichier = (isset($_POST['fileNamePlugin']))? $_POST['fileNamePlugin'] : "test.png";
    // si un fichier du même nom existe déjà dans ce dossier, on le supprime
    supprimeFichierExistant($uploadDir, $nomFichier);
    // on enregistre le fichier dans ce dossier
    $file = $uploadDir."/".$nomFichier;
    $success = file_put_contents($file, $decodedData);
}
else{
    echo "no such folder";
}

function supprimeFichierExistant($dir, $fileName){
    //si le dossier pointe existe
    if (is_dir($dir)) {
        // s'il contient quelque chose
        if ($dh = opendir($dir)) {
            // boucler tant que quelque chose est trouve
            while (($file = readdir($dh)) !== false) {
                if ($file == $fileName) {
                    if (file_exists($dir . "/" . $file)) {
                        unlink($fileName); //suppression de l'image
                    }
                }
            }
            closedir($dh);
        }
    }
}
